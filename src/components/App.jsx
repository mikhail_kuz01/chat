import React, { useReducer, useEffect } from 'react'
import axios from 'axios'

import reducer from '../reducer.js'
import socket from '../socket.js'

import SignIn from './SignIn/SignIn.jsx'
import Community from './Community/Community.jsx'

const App = () => {
    const [state, dispatch] = useReducer(reducer, {
        joined: false,
        roomId: null,
        userName: null,
        users: [],
        messages: []
    })

    const setUsers = (users) => {
        dispatch({
            type: 'SET_USERS',
            payload: users
        })
    }

    const addMessage = (message) => {
        dispatch({
            type: 'NEW_MESSAGE',
            payload: message
        })
    }

    const onSignIn = async (obj) => {
        dispatch({
            type: 'JOINED',
            payload: obj
        })
        socket.emit('ROOM:JOIN', obj)
        const { data } = await axios.get(`/rooms/${obj.roomId}`)
        setUsers(data.users)
    }

    useEffect(() => {
        socket.on('ROOM:SET_USERS', setUsers)
        socket.on('ROOM:NEW_MESSAGE', addMessage)
    }, [])

    return (
        <div>
            {!state.joined ? <SignIn onSignIn={onSignIn} /> : <Community {...state} onAddMessage={addMessage} userId={socket.id} />}
        </div>
    )
}

export default App