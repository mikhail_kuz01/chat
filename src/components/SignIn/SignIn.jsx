import React, { useState } from 'react'
import axios from 'axios'

import socket from '../../socket.js'

const SignIn = ({ onSignIn }) => {
    const [roomId, setRoomId] = useState('')
    const [userName, setUserName] = useState('')

    const onEnter = async () => {
        const obj = {
            roomId,
            userName
        }

        await axios.post('/rooms', obj)
        onSignIn(obj)
}

    return (
        <div className='signIn'>
            <h2 className='signIn__title'>Sign In</h2>
            <form onSubmit={e => {e.preventDefault(); onEnter()}} className='signIn__form'>
                <input className='signIn__form_inpt' type='text' placeholder='Room ID' value={roomId}  pattern='[\w]{1,}' onChange={(e) => setRoomId(e.target.value)} required />
                <input className='signIn__form_inpt' type='text' placeholder='Name' value={userName} pattern='[\w]{1,}' onChange={(e) => setUserName(e.target.value)} required />
                <button className='signIn__form_btns' type='submit'>Sign in</button>
            </form>
        </div>
    )
}

export default SignIn