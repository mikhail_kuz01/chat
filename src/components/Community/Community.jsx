import React, { useState, useEffect, useRef } from 'react'

import socket from '../../socket.js'

const Community = ({ roomId, users, messages, userName, onAddMessage, userId }) => {
    const [messageValue, setMessageValue] = useState('')
    const messagesRef = useRef(null)

    const onSendMessage = () => {
        socket.emit('ROOM:NEW_MESSAGE', {
            userId,
            userName,
            roomId,
            text: messageValue
        })
        onAddMessage({ userId, userName, text: messageValue })
        setMessageValue('')
        document.querySelector('.chat__send__form_text').innerHTML = ''
    }

    const writeMessage = (text, id) => {
        setTimeout(() => {
            document.getElementsByClassName('message_text').item(++id).innerHTML = text
        }, 1)
    }

    useEffect(() => {
        messagesRef.current.scrollTo(0, 99999)
    }, [messages])

    return (
        <div className='community'>
            <div className='sidebar'>
                <h2 className='sidebar__title'>ROOM: <span className='roomId'>{roomId}</span></h2>
                
                <h2 className='sidebar__title'>Users ({users && users.length}):</h2>
                <ul className='sidebar__users'>
                    {users && users.map((user, id) => (
                        <li className='sidebar__users_user' key={id}>{user}</li>
                    ))}
                </ul>
            </div>
            <div className='chat'>
                <div ref={messagesRef} className='chat__messages'>
                    <div className='message'>
                        <div className='message_text'>Welcome to the {roomId} room :)</div>
                        <div className='message_autor'>System</div>
                    </div>
                    {messages && messages.map((message, id) => (
                        <div key={id} className={(message.userId == userId) ? 'message myMessage' : 'message'}>
                            <div className='message_text'>{writeMessage(message.text, id)}</div>
                            {message.userId != userId && <div className='message_autor'>{message.userName}</div>}
                        </div>
                    ))}
                </div>
                <div className='chat__send'>
                    <form 
                        onSubmit={(e) => {
                            e.preventDefault()
                            messageValue && onSendMessage()
                        }} 
                        className='chat__send__form'
                    >
                        <div className='chat__send__form_text' contentEditable='true' aria-multiline='true'></div>
                        <button className='chat__send__form_btns' type='submit' onClick={() => setMessageValue(document.querySelector('.chat__send__form_text').innerHTML)}>Send</button>
                    </form>

                </div>
            </div>
        </div>
    )
}

export default Community