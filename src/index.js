import React from 'react'
import ReactDOM from 'react-dom'

import App from './Components/App.jsx'

import './styles/index.scss'

ReactDOM.render(
    <App />,
    document.getElementById('root')
)